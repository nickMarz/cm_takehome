This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

To run project
### `yarn`
#### then 
### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Branches
  * Master, just some cleanup from the CRA
  * react_navigation
  * sans_react_navigation

## Files
  * React Version
    * ./src/NavigationSlider.js
    * ./src/navigation_slider.scss

  * Vanilla JS Version
    * ./public/navigation_slider.js
    * ./src/navigation_slider.scss